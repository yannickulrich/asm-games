SECTION .data
clcCmd db 1BH, '[2J'
movCmd db 1BH, '[00;00H'
paddleSym db 124, 10, 124, 10

SECTION .bss
termios:        resb  0  ;36  ; get 36 bytes for termios
tv:             resb  0  ;16  ; 2*64 bit
buf:            resb 25  ; 1 byte buffer


%define GAMEHEIGHT 20
%define GAMEWIDTH 80

SECTION .text
global  _start
_start:
%include "pong-core.asm"
