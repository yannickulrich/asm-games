%include "ioctl.asm"
    ; We use the ESI for the game state. The lowest two
    ; bits are the speed.
    ;   ball speed (0: --, 1: -+, 2: +-, 3: ++)
    ; Bits 2-7 are the cursor Y. Bits 8-13 are the ball's Y
    ; and bits 14-21 the ball's X
    mov esi, 23

_loop:
    ; move ball

    ; Y movement
    mov ebx, 1<<7     ; +1 for the bits 8-13

    mov eax, esi
    and eax, 1
    jnz _ballSY
    neg ebx           ; negate speed
_ballSY:
    add esi, ebx

    ; X movement
    mov ebx, 1<<14    ; +1 for the bits 14-21

    mov eax, esi
    and eax, 2
    jnz _ballSX
    neg ebx           ; negate speed
_ballSX:
    add esi, ebx


_stdin:
    ; read stdin
    mov byte [buf], 0
    mov eax, 3        ; read syscall
    xor ebx, ebx      ; fd=stdin
    mov ecx, buf     ; target
    mov edx, 10
    int 80h
    mov byte al, [buf]

    ; check for q
    cmp al, 113       ; cmp with 'q'
    je _end

    ; paddle movement
    cmp al, 119       ; cmp with 'w'
    je _moveup
    cmp al, 115       ; cmp with 's'
    je _movedown

    jmp _collisionDet

_moveup:
    sub esi, 4        ; this decrements the higher bits
    jmp _collisionDet

_movedown:
    add esi, 4        ; this increments the higher bits
    jmp _collisionDet

_collisionDet:
_paddleCY:
    mov ecx, esi
    and ecx, 124      ; this gets the cursor Y
    jz _paddleCYT
    cmp ecx, (GAMEHEIGHT-3)<<2
    je _paddleCYB
    jmp _paddleCYDone

_paddleCYT:
    add esi, 4
    jmp _paddleCYDone
_paddleCYB:
    sub esi, 4
    jmp _paddleCYDone


_paddleCYDone:
_ballCY:
    mov ebx, 128
    mov eax, esi
    and eax, 31<<7    ; this gets the ball's Y
    jz _ballCYT
    cmp eax, GAMEHEIGHT<<7
    je _ballCYB
    jmp _ballCYDone

_ballCYB:
    neg ebx
_ballCYT:
    add esi, ebx
    xor esi, 1

_ballCYDone:
_ballCX:
    mov ebx, -(1<<14)
    mov eax, esi
    and eax, 127<<13
    jz _ballCXL
    cmp eax, GAMEWIDTH<<13
    je _ballCXR
    jmp _ballCXDone

_ballCXL:
    mov eax, esi
    shr eax, 7
                      ; since _paddleCY, contains the cursor's Y
    shr ecx, 2        ; .. and shifts it down
    sub eax, ecx
    and eax, ~3
    jnz _end
    neg ebx
_ballCXR:
    xor esi, 2
    add esi, ebx


_ballCXDone:
_draw:
    ; Draw clear cmd
    mov eax, [clcCmd]
    mov dword [buf], eax

    ; Draw ball
    mov eax, [movCmd]
    mov dword [buf+4], eax
    mov dword [buf+13], eax

    mov eax, [movCmd+4]
    mov dword [buf+8], eax
    mov dword [buf+17], eax

    mov byte [buf+12], 111 ; o sign

    ; Draw paddle
    mov dword eax, [paddleSym]
    mov dword [buf+21], eax
    ;mov byte [buf+21], 124 ; | sign
    ;mov byte [buf+22], 10
    ;mov byte [buf+23], 124
    ;mov byte [buf+24], 10

    ; Adapt location for paddle
    mov eax, esi         ; rax is what we divide
    shr eax, 2
    and eax, 31
    mov ebx, 15
    call itoa

    ; Adapt location for ball
    mov eax, esi
    shr eax, 7          ; rax is what we divide
    and eax, 31
    mov ebx, 6
    call itoa

    mov eax, esi
    shr eax, 13
    and eax, 127
    mov ebx, 9
    call itoa

    mov eax, 4        ; write syscall
    mov ebx, 1        ; fd=stdout
    mov ecx, buf
    mov edx, 25
    int 80h

    ; sleep
    mov dword [tv], 0
    mov dword [tv+4], 100000000
    mov eax, 0xa2     ; nanosleep
    mov ebx, tv       ; sleeps spec
    xor ecx, ecx
    int 80h

    jmp _loop

_end:
    mov eax, 1
    xor ebx, ebx
    int 80h


itoa:
    xor edx, edx        ; clear target
    mov ecx, 10
    idiv ecx            ; excute division

    add eax, 48         ; rax is the ten place, conv to ASCII
    add edx, 48         ; rdx is the one place, conv to ASCII
    mov ecx, buf
    add ecx, ebx
    mov byte [ecx], al
    mov byte [ecx+1], dl

    ret
