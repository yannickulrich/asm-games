SECTION .data
clcCmd db 1BH, '[2J', 1BH, '[00;00Hx'
movCmd db 1BH, '[00;00H#'
random  dd  0   ; Where the random number will be stored

%define TAILLENGTH 16
tail db 0x18, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, \
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00

SECTION .bss
termios:        resb  0  ;36  ; get 36 bytes for termios
tv:             resb  0  ;16  ; 2*64 bit
buf:            resb  1  ; 1 byte buffer

SECTION .text
global  _start
_start:
%include "snake-core.asm"
