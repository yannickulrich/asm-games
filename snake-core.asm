%include "ioctl.asm"
_init:
    ; We use the ESI for the game state. The first eight bytes
    ; are the position of the apple. The next two bits are the current
    ; direction:
    ; 00: up, 01: down, 10: left, 11: right
    mov esi, 0xe3
    mov esp, tail
    mov ebp, tail

_loop:
    jmp _stdin

_stdin:
    ; read stdin
    mov byte [buf], 0
    mov eax, 3        ; read syscall
    xor ebx, ebx      ; fd=stdin
    mov ecx, buf     ; target
    mov edx, 10
    int 80h
    mov byte al, [buf]

    ; check for q
    cmp al, 113       ; cmp with 'q'
    je _end

    cmp al, 'w'
    je _moveup
    cmp al, 's'
    je _movedown
    cmp al, 'a'
    je _moveleft
    cmp al, 'd'
    je _moveright
    jmp _motion

_moveup: ; set 00
    and esi, 0xfffffcff
    jmp _motion
_movedown: ; set 01
    and esi, 0xfffffcff
    or esi, 0100000000b
    jmp _motion
_moveleft: ; set 10
    and esi, 0xfffffcff
    or esi, 1000000000b
    jmp _motion
_moveright: ; set 11
    and esi, 0xfffffcff
    or esi, 1100000000b
    jmp _motion

_motion:
    mov byte bl, [esp]
    mov eax, esi
    cmp al, bl
    je _ateapple
    dec ebp
    jmp _movehead

_ateapple:
    ; we now need to move the apple. This syscalls time(2) which
    ; is NOT Y2K36 safe!
    mov eax, 13
    mov ebx, random
    int 80h
    mov eax, [ebx]
    and eax, 0xff
    and esi, 0xffffff00
    or esi, eax
    ; the syscall mucked up eax and ebx
    mov eax, esi
    mov byte bl, [esp]

_movehead:
    dec esp

_checkHead:
    cmp esp, tail
    jnc _checkTail
    add esp, TAILLENGTH

_checkTail:
    cmp ebp, tail
    jnc _motionmain
    add ebp, TAILLENGTH

_motionmain:

    mov eax, esi
    shr eax, 8
    and eax, 3
    jmp [.JumpTab + 4*eax]
.JumpTab: DD _motionup, _motiondown, _motionleft, _motionright
_motionup:
    sub bl, 16
    jmp _draw
_motiondown:
    add bl, 16
    jmp _draw
_motionleft:
    sub bl, 1
    jmp _draw
_motionright:
    add bl, 1
    jmp _draw

_draw:
    mov byte [esp], bl

    ; Modify clear cmd to include apple
    mov eax, esi
    and eax, 0xf0
    shr al, 4
    mov ax, word [_numbers + 2*eax]
    mov word [clcCmd+6], ax

    ; copy y
    mov eax, esi
    and eax, 0x0f
    mov ax, word [_numbers + 2*eax]
    mov word [clcCmd+9], ax

    ; Draw clear cmd
    mov eax, 4        ; write syscall
    mov ebx, 1        ; fd=stdout
    mov ecx, clcCmd
    mov edx, 13
    int 80h

    ; We use edi as the runner
    ; mov byte [movCmd+8], '#'
    mov edi, esp
_draw1:
    ; draws instr. at [edi], will muck up eax to edx
    ; copy x
    xor ax, ax
    mov al, byte [edi]

    ; collision detection
    cmp edi, esp
    je _finishx
    cmp al, [esp]
    ; mov byte [movCmd+8], '.'
    je _end
    ; // collision detection
_finishx:
    and al, 0xf0
    shr al, 4
    mov ax, word [_numbers + 2*eax]
    mov word [movCmd+2], ax

    ; copy y
    xor ax, ax
    mov al, byte [edi]
    and al, 0x0f
    mov ax, word [_numbers + 2*eax]
    mov word [movCmd+5], ax

    mov eax, 4        ; write syscall
    mov ebx, 1        ; fd=stdout
    mov ecx, movCmd
    mov edx, 9
    int 80h

_draw1done:
    cmp ebp, edi
    je _drawdone

    inc edi
    cmp edi, tail+TAILLENGTH
    jc _draw1
    sub edi, TAILLENGTH
    jmp _draw1
_drawdone:


    ; sleep
    mov dword [tv], 0
    mov dword [tv+4], 100000000
    mov eax, 0xa2     ; nanosleep
    mov ebx, tv       ; sleeps spec
    xor ecx, ecx
    int 80h

    jmp _loop

_end:
    mov eax, 1
    xor ebx, ebx
    int 80h

_numbers dw "00", "01", "02", "03", "04", "05", "06", "07", \
            "08", "09", "10", "11", "12", "13", "14", "15"
