BITS 32
            org 0x08048000

ehdr:
    db 0x7F, "ELF"             ; e_ident magic
                               ; this is part of e_ident
                               ; but doesn't matter
                               ; so let's put movCmd here
paddleSym db 124, 10, 124, 10
movCmd db 1BH, '[00;00H'
    dw 2                       ; e_type = ET_EXEC
    dw 3                       ; e_machine = x86
clcCmd db 1BH, '[2J'           ; technically, this is dd e_version but it's not checked
    dd _start                  ; e_entry
    dd phdr - $$               ; e_phoff
    dd 0                       ; e_shoff -- we don't have a section header
    dd 0                       ; e_flags
    dw ehdrsize                ; e_ehsize
    dw phdrsize                ; e_phentsize
  phdr:
    dd 1                       ; e_phnum       ; p_type
                               ; e_shentsize
    dd 0                       ; e_shnum       ; p_offset
                               ; e_shstrndx
ehdrsize      equ     $ - ehdr

    dd $$                      ; p_vaddr
    dd $$                      ; p_paddr
    dd filesize                ; p_filesz
    dd filesize                ; p_memsz
    dd 5                       ; p_flags
    dd 0x1000                  ; p_align

phdrsize      equ     $ - phdr

%define buf 0x8048020
%define termios 0x8048020
%define tv 0x8048020
%define GAMEHEIGHT 20
%define GAMEWIDTH 80

_start:
    mov eax, 7dh
    mov ebx, 0x8048000
    mov ecx, 25
    mov edx, 2|3
    int 80h

%include "pong-core.asm"


filesize      equ     $ - $$
