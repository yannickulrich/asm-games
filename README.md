# QR Code Games

Games written in x86 assembly that are small enough to fit into a QR code.

<table>
    <tr>
        <td>
            <img src="https://gitlab.com/yannickulrich/qr-games/-/jobs/artifacts/root/raw/pong.png?job=build">
        </td>
        <td>
            <img src="https://gitlab.com/yannickulrich/qr-games/-/jobs/artifacts/root/raw/snake.png?job=build">
        </td>
    </tr>
    <tr>
        <td>Pong</td>
        <td>Snake</td>
    </tr>
</table>

## Running it
You just need [NASM](https://nasm.us/) to compile this and `qrencode` if you want to generate the QR code.
```shell
$ make
$ ./pongmin
# or
# ./snakemin
```
Note that this will break your terminal, you will have to run `reset` afterwards to restore a proper input.
You can also run the QR code
```shell
$ zbarimg --raw -q pong.png | base64 --decode > /tmp/game
# or
$ zbarcam -q --raw | base64 --decode > /tmp/game
$ chmod +x /tmp/game
$ /tmp/game
```

## The challenge
A while ago I saw the following challenge somewhere
> Rules:
> * the game needs to fit into a QR code.
    This means 2953 bytes of [raw binary](https://en.wikipedia.org/wiki/ISO/IEC_8859-1) or 4296 bytes of text.
    Since the binary format is not particularly portable, we prefer to use a Base64 encoding which means 3222 bytes.
> * the game needs to be included in the QR code and not just link to it.
    Otherwise it's of course trivial by just linking to any web game
> * the game needs to be reasonably self-sufficient and have as few dependencies as possible.
>   Otherwise we could write a game in minified javascript which relies on a huge engine.
> * the game should run in normal user space, i.e. [BootChess](https://www.chessprogramming.org/BootChess) (which at 468 bytes fits into the boot sector) is not eligible.

The OP did not solve the challenge to my satisfaction (but they've also decided to write a GUI app).

## Strategy
We will use the following tricks:

 * Write a terminal application will make for a smaller application as we won't have to include a graphics stack.
   However, it means we have to mess with [`ioctl`, `fcntl`](#ioctl-and-fcntl) and [ANSI sequences](#ansi-sequences).
 * Write it directly in x86 assembly will make it smaller.
 * Use [syscalls](#syscalls) instead of `libc` means we have more control over registries.
 * Use registers instead of memory to store (much of) the game state
   (instructions involving memory need comparatively much machine code).
 * Use x86 instead of x86-64 since memory addresses are shorter.
 * Store data in the [ELF header](#elf-header) which is surprisingly flexible.

The makefile builds two version of both games
 * `pong` and `snake`: proper ELF headers, can be debugged with `gdb` when compiled with `DEBUG=1`
 * `pongmin` and `snakemin`: minimised files that cannot be debugged.

### `ioctl` and `fcntl`
Normally when reading from `stdin`, the typed letters are echoed into the terminal and only passed to the program once a line-delimiter character is provided.
This odiously won't work for a game, we need the input immediately (or nothing) and not echo it.
For this we need non-canonical input without echo which is done with the `ioctl` syscall (0x36 on 32bit linux).
We need to fetch the current `termios` configuration
```nasm
mov eax, 36h      ; ioctl syscall
xor ebx, ebx      ; fd=stdin
mov ecx, 5401h    ; tcgets
mov edx, termios  ; output
int 80h
```
make the modification to `termios->c_lflag` which is at offset 12
```c
termios->c_lflag &= ~(ECHO | CHONL);
```
```nasm
; and 12th byte with ~(ECHO|CANON)
and dword [termios+12], ~10
```
and set the `ioctl`
```nasm
; set ioctl
mov eax, 36h      ; ioctl syscall
mov ebx, 0        ; fd=stdin
mov ecx, 5402h    ; tcgets
mov edx, termios  ; output
int 80h
```
Note that since the kernel returns our registers as they were before, we can reduce this to three instructions
```nasm
; set ioctl
mov eax, 36h      ; ioctl syscall
inc ecx
int 80h
```
Next, we need to use `fcntl` to disable blocking.
The strategy is the same: get the `fcntl` configuration, modify it, and returning it
```nasm
; get fcntl
mov eax, 37h      ; fcntl syscall
mov ebx, 0        ; fd=stdin
mov ecx, 3        ; F_GETFL
xor edx, edx      ; arg
int 80h           ; answer in rax
; modify
mov edx, eax      ; copy ans
or edx, 0x800     ; O_NONBLOCK
; set fcntl
mov eax, 37h      ; fcntl syscall
mov ebx, 0        ; fd=stdin
mov ecx, 4        ; F_SETFL
int 80h           ; answer in rax
```
Which can be abbreviated to
```nasm
mov eax, 37h      ; fcntl syscall
mov ecx, 3        ; F_GETFL
xor edx, edx      ; arg
int 80h           ; answer in rax
mov edx, eax      ; copy ans
or edx, 0x800     ; O_NONBLOCK
mov eax, 37h      ; fcntl syscall
inc ecx
int 80h           ; answer in rax
```

### ANSI Sequences
We want to print at any location in the terminal which we can do using ANSI sequences.
We need
```c
printf("\033[2J");           // clears the screen
printf("\033[%d;%dH", x, y); // moves the cursor
```
Since we don't have `printf` we either need to build [our own `itoa`](https://gitlab.com/yannickulrich/qr-games/-/blob/root/pong-core.asm#L187-199) or use a [look-up table](https://gitlab.com/yannickulrich/qr-games/-/blob/root/snake-core.asm#L194-195).

### Syscalls
We can do i/o using syscalls instead of `printf` and `scanf`.
To write a prepared buffer to `stdout`
```nasm
mov eax, 4        ; write syscall
mov ebx, 1        ; fd=stdout
mov ecx, buf
mov edx, 25
int 80h
```
and to read a single byte
```nasm
    ; read stdin
    mov byte [buf], 0
    mov eax, 3        ; read syscall
    xor ebx, ebx      ; fd=stdin
    mov ecx, buf     ; target
    mov edx, 10
    int 80h
    mov byte al, [buf]
```
The last instruction puts the character we've read into the lowest 8bit of eax.

### ELF header
This is the most important feature.
ELF executables have quite a large overhead because the executable is split into metadata, `.data` (initialised variables), `.bss` (uninitialised variables), and `.text` (code).
Even an otherwise empty program
```nasm
SECTION .data
clcCmd db 1BH, '[2J'
movCmd db 1BH, '[00;00H'

SECTION .bss
termios:        resb 25

SECTION .text
global  _start
_start:
    mov eax, 1
    xor ebx, ebx
    int 80h
```
is 8432 bytes which is already way to big.
We can reduce this by manually managing the ELF file.

The buffer variables that we need at runtime (the biggest being `termios` at 25 bytes) can be anywhere in memory as long as we can write to it, incl. the header and code we no longer need to execute (such as initialisation).
For this, we need to mark the memory page read/write
```nasm
mov eax, 7dh
mov ebx, 0x8048000
mov ecx, 25
mov edx, 2|3
int 80h
```
which gains us more space than it cost.

Further, we can store pre-defined variables (such as the escape sequences) in the [ELF headers](https://en.wikipedia.org/wiki/Executable_and_Linkable_Format#File_header).
Clearly, this shouldn't work, these fields mean something and we can't just put random data in them.
However, it turns out that current Linux kernels (at least 6.0.7) are surprisingly lenient about this.

A normal (but minimal) ELF header looks like this
```nasm
BITS 32
            org 0x08048000

ehdr:
    db 0x7F, "ELF"             ; e_ident magic
    db 1                       ; e_ident, class (32 bit)
    db 1                       ; e_ident, endiness
    db 3, 0                    ; e_ident, ABI (Linux)
    db 0, 0, 0, 0, 0, 0, 0     ; e_ident, padding

    dw 2                       ; e_type = ET_EXEC
    dw 3                       ; e_machine = x86
    dd 1                       ; e_version

    dd _start                  ; e_entry
    dd phdr - $$               ; e_phoff
    dd 0                       ; e_shoff -- we don't have a section header
    dd 0                       ; e_flags
    dw ehdrsize                ; e_ehsize
    dw phdrsize                ; e_phentsize
  phdr:
    dd 1                       ; e_phnum       ; p_type
                               ; e_shentsize
    dd 0                       ; e_shnum       ; p_offset
                               ; e_shstrndx
ehdrsize      equ     $ - ehdr

    dd $$                      ; p_vaddr
    dd $$                      ; p_paddr
    dd filesize                ; p_filesz
    dd filesize                ; p_memsz
    dd 5                       ; p_flags
    dd 0x1000                  ; p_align

phdrsize      equ     $ - phdr

_start:
    <code goes here>
```
It turns out that only the magic of `e_ident` matters, the rest can be whatever.
The same is true for `e_version`.
This means we have 12 bytes in `e_ident` and another 4 bytes in `e_version` where we can store the escape sequences
```nasm
ehdr:
    db 0x7F, "ELF"             ; e_ident magic
                               ; this is part of e_ident
                               ; but doesn't matter
                               ; so let's put movCmd here
paddleSym db 124, 10, 124, 10
movCmd db 1BH, '[00;00H'
    dw 2                       ; e_type = ET_EXEC
    dw 3                       ; e_machine = x86
clcCmd db 1BH, '[2J'           ; technically, this is dd e_version but it's not checked
```

For more on these hacks, I recommend reading [*Smallest x86 ELF Hello World* (142 bytes)](http://timelessname.com/elfbin/) and [*Teensy Files* (42 bytes)](http://www.muppetlabs.com/~breadbox/software/tiny/).
