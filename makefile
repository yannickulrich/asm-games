DEBUG := 0

ASM=nasm
AFLAGS=-f elf

LD=ld
LFLAGS= -m elf_i386

ifeq ($(DEBUG), 1)
AFLAGS+=-g -F dwarf
else
LFLAGS+=--strip-all
endif

bins=pong pongmin snake snakemin
imgs=pong.png snake.png

all: $(bins) $(imgs)
# debug

pong.o: pong.asm pong-core.asm
		$(ASM) $(AFLAGS) -o $@ $<
snake.o: snake.asm snake-core.asm
		$(ASM) $(AFLAGS) -o $@ $<

%: %.o
		$(LD) $(LFLAGS) -o $@ $^

pongmin: pong-elf.asm
		$(ASM) -f bin -o $@ $<
		chmod +x $@

snakemin: snake-elf.asm
		$(ASM) -f bin -o $@ $<
		chmod +x $@

%.png: %min
		cat $< | base64 -w 0 | qrencode -o $@

%.json: %min
		echo "{\"schemaVersion\": 1,\"label\": \"$(basename $@)\", \"message\": \"$$(cat $< | wc -c ) bytes\"}" > $@

run: snake
		alacritty -e bash -c './$< ; read -n 1'

clean:
		rm -f *.o $(bins) *.png *.json
