FROM alpine:edge
RUN apk add --no-cache nasm make binutils-x86 libqrencode &&\
    ln -s /usr/i586-alpine-linux-musl/bin/ld /usr/bin/ld
