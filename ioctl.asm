    ; get ioctl
    mov eax, 36h      ; ioctl syscall
    xor ebx, ebx      ; fd=stdin
    mov ecx, 5401h    ; tcgets
    mov edx, termios  ; output
    int 80h

    ; and 12th byte with ~(ECHO|CANON)
    and dword [termios+12], ~10

    ; set ioctl
    mov eax, 36h      ; ioctl syscall
   ;mov ebx, 0        ; fd=stdin
   ;mov ecx, 5402h    ; tcgets
    inc ecx
   ;mov edx, termios  ; output
    int 80h

    ; get fcntl
    mov eax, 37h      ; fcntl syscall
   ;mov ebx, 0        ; fd=stdin
    mov ecx, 3        ; F_GETFL
    xor edx, edx      ; arg
    int 80h           ; answer in rax
    mov edx, eax      ; copy ans
    or edx, 0x800     ; O_NONBLOCK
    mov eax, 37h      ; fcntl syscall
   ;mov ebx, 0        ; fd=stdin
   ;mov ecx, 4        ; F_SETFL
    inc ecx
    int 80h           ; answer in rax


